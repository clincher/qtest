#!/usr/bin/python
"""
Prints our IP address or "not connected" if we are offline
"""
from urllib2 import urlopen, URLError

try:
    # send request to a service which returns back our raw ip
    print(urlopen('http://ip.42.pl/raw').read())
except URLError:
    print("not connected")
