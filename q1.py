#!/usr/bin/python
"""
a. Scrapping consists of 2 main processes: 1) managing links to visit;
2) parse html and extract data. It's important to make requests asynchronously.
Optionally sometimes it's necessary to launch pages in a browser to run
javascript code and perform interactions with pages before it will be possible
to access data.
In my experience I parsed HTML/XML (2nd process) with lxml library and
Selenium for interacting with dynamic web pages. I chose lxml over
BeatifulSoup because it's good enough. I've learned about Scrapy - a library for
making a crawler, but I didn't have any task when I needed it yet.

b. Tuple and list are both sequences, but tuple is immutable and list can be
modified. For example, tuple can be a key of a dictionary since it hashable
and list can not.

"""
# c.
for i in xrange(1, 1001):
    if not (i % 7 or i % 6):
        print("Filam Technology")
    elif not i % 7:
        print("Filam")
    elif not i % 6:
        print("Technology")
    else:
        print(i)

"""
d. Generators are used to run over loops without creating massive structures 
in memory (such as lists or other sequences). Instead they know how to return 
next item in a row. I already used generator for c
"""
