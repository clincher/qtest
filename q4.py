"""

"""
import json

from q3 import extract_data_from_virginia_case


URL_PATTERN = "https://eapps.courts.state.va.us/cav-public/caseInquiry" \
              "/showCasePublicInquiry?caseId={}"

cases = []
for i in xrange(23800, 23851):
    cases.append(extract_data_from_virginia_case(URL_PATTERN.format(i)))


with open('q4.json', 'wb') as outfile:
    json.dump(cases, outfile, indent=2)
