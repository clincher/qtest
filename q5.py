#!/usr/bin/python
"""
Downloads specific web pages and saves them to files
"""
import urllib
import urllib2


CASE_SEARCH_DISCLAIMER_URL = "http://casesearch.courts.state.md.us/casesearch/"
CASE_SEARCH_FORM_URL = \
    "http://casesearch.courts.state.md.us/casesearch/processDisclaimer.jis"

with open('q5-1.html', 'wb') as outfile:
    outfile.write(urllib2.urlopen(CASE_SEARCH_DISCLAIMER_URL).read().strip())


# We can't click any checkbox on HTML page with Python/BeautifulSoup
# We could have done it with Selenium tho, or just send POST-request as the
# previous page does
with open('q5-2.html', 'wb') as outfile:
    data = urllib.urlencode({'disclaimer': 'Y', 'action': 'Continue'})
    outfile.write(urllib2.urlopen(CASE_SEARCH_FORM_URL, data).read().strip())
