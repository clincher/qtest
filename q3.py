#!/usr/bin/python
"""
Collects data for specific Virginia court case and saves it to the json file
"""
import json
import urllib2

from bs4 import BeautifulSoup


def extract_data_from_virginia_case(url):
    """
    Receives html from given url, parse it and extract needed fields to dict
    :param url: str
    :return: dict
    """
    soup = BeautifulSoup(urllib2.urlopen(url), 'html.parser')
    return {
        'appellant_names': list(
            i.findAll("td")[0].text.strip() for i in soup.findAll(
                id="listAllPartiesAPL")[0].findAll("tr")),
        'appellee_names': list(
            i.findAll("td")[0].text.strip() for i in soup.findAll(
                id="listAllPartiesAPE")[0].findAll("tr")),
        'case_number': soup.findAll(
            "input", attrs={'name': "caseNumber"})[0].attrs['value'],
        'case_date': soup.findAll(
            "input", attrs={'name': "noticeOfAplDt"})[0].attrs['value']
    }


CASE_ID_URL = 'https://eapps.courts.state.va.us/cav-public/caseInquiry/' \
              'showCasePublicInquiry?caseId=23811'


if __name__ == "__main__":
    # collect data for the case
    data = extract_data_from_virginia_case(CASE_ID_URL)
    # save data to file
    with open('q3.json', 'wb') as outfile:
        json.dump(data, outfile, indent=2)
